var mb= this;
//var cb = qdaq.findChild('PrSet');
var offset = 3000; // LabJack MODBUS address DIO0_EF_READ_A
//var offset = 3100; // LabJack MODBUS address DIO0_EF_READ_A_AND_RESET
var buff = mb.read(offset,2);
var DC = 0;
if(buff.length == 4){
    var counts = buff.readUInt16LE(2) + buff.readUInt16LE()*65536;
    DC = counts - mb.PrevCnts;
    mb.PrevCnts = counts;
}

// *** FOR TESTING !!
//if (mb.beamOn) DC = 10;


mb.TotalCnts += DC;
//mb.TotalCnts += DC

mb.Ib.push(DC);
mb.IbSum.push(mb.TotalCnts);
if (mb.presetEnable) {
    if (mb.TotalCnts >= mb.presetCounts) {
        mb.write(2002,0); 
        mb.beamOn = 0;
    }
}

 

//mbctr.IbChan.push(mbctr.channel);
//mb.beamStatus.push(mbctr.beamOn);

