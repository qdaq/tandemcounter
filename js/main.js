/*****************************.
 * TandemApp
 * 
 * V0.1, EM, July 2023
 * V0.2, GA+EM, Oct 2023
 * V0.3, GA+EM, Feb 2024
 * 
 */


function create() {
        // create main loop
        var mainLoop = qdaq.appendChild(new QDaqLoop("mainLoop"));
        mainLoop.createLoopEngine(); // main loop script engine
        // mainLoop.period = config.mainLoopPeriod;
        mainLoop.period = 1000;

        // add main loop clock
        var t = mainLoop.appendChild(new QDaqChannel("t"));
        t.type = "Clock";
        t.format = "Time";
        t.signalName = 'meas. time';
        t.unit = 's';
        
        // create modbus connection to counter device with modbus ifc
        var mb = qdaq.mainLoop.appendChild(new QDaqModbusTcp("mb"));
        mb.host = "10.0.5.14";
        mb.port = 502;
        // create beam counter device
        var beamctr = mainLoop.appendChild(new QDaqDevice("beamctr"));
        beamctr.interface = mb;

        // create beam counter device
        //var beamctr = mainLoop.appendChild(new QDaqJob("beamctr"));
     
        beamctr.setQDaqProperty('PrevCnts',0);
        beamctr.setQDaqProperty('TotalCnts',0);
        beamctr.setQDaqProperty('presetCounts',0);
        beamctr.setQDaqProperty('presetEnable',0);
        beamctr.setQDaqProperty('beamOn',0);
        
        

        // create beam channel 
        var Ib = beamctr.appendChild(new QDaqChannel("Ib"));
        Ib.signalName = "Beam Current";
        //Ib.type = 'Random';
        Ib.format = 'FixedPoint';
        Ib.digits = 2;
        Ib.unit = 'nA';
        Ib.multiplier = 1e9;
        //Ib.depth = Math.round(1000/config.mainLoopPeriod); // 1s running average
        Ib.averaging = 'Running';
/// !!!!!! CHAnGE IbSum to TotalCnts if possible !!!!!!!!

        var IbSum = beamctr.appendChild(new QDaqChannel("IbSum"));
        IbSum.signalName = "Total Beam Current";
        IbSum.format = 'FixedPoint';
        IbSum.digits = 2;
        IbSum.unit = 'nA';
        IbSum.multiplier = 1;
        Ib.averaging = 'Running';

        // create temp data 5 mins
        var rtData = mainLoop.appendChild(new QDaqDataBuffer("rtData"));
        rtData.circular = true;
        rtData.backBufferDepth = 4;
        rtData.capacity = 5*60; // 5 min buffer

        // create buffered data
        var bfData = mainLoop.appendChild(new QDaqDataBuffer("bfData"));
        bfData.backBufferDepth = 4;

        var chList = [
            qdaq.mainLoop.t,
            qdaq.mainLoop.beamctr.Ib,
            qdaq.mainLoop.beamctr.IbSum
        ];
        rtData.channels = chList;
        bfData.channels = chList;

        //

}

function createUi() {
        // load the top level ui window
        var mainLoop = qdaq.findChild("mainLoop");
        var mainUi = ui.loadTopLevelUi('js/main.ui','mainUi');

        var led = ui.mainUi.findChild('ledBeam');
        qdaq.mainLoop['updateWidgets()'].connect(led['toggle()']);

        ui.bind(mainLoop.beamctr.Ib,mainUi.findChild("Ib"));
        ui.bind(mainLoop.beamctr.IbSum,mainUi.findChild("IbSum"));
        //ui.bind.(mainLoop.beamctr.StatusMsg, mainUi.findChild("edtStatus"));

        // Connect button
        var btn = mainUi.findChild('btConnect'); 
        btn['toggled(bool)'].connect(start); // alternate start function

        // Start/Stop button
        var btn = mainUi.findChild('btBeamOn'); 
        btn['toggled(bool)'].connect(beamOn); 

        // Range Scale
        btn = mainUi.findChild('cbRange');
        btn['currentIndexChanged(int)'].connect(rangeChanged);

        // dataTabs
        // realTime data/bufferedData
        var rtData = qdaq.findChild('rtData');
        var bfData = qdaq.findChild('bfData');
       
         // Reset button
        btn = mainUi.findChild("btReset");
        btn['clicked()'].connect(reset);

        // About button/Helpfile
        var btn = mainUi.findChild('btHelp'); 
        btn['clicked()'].connect(popup_funct); 

        updatePlot();

        // plot window
        // 1. buttons under plotwindow
        btn = mainUi.findChild('btRt');
        btn['toggled(bool)'].connect(updatePlot);
        // 2. plotwindow
        mainLoop.updateWidgets.connect(mainUi.findChild("plotIbvst").replot);
        mainLoop.updateWidgets.connect(updateStatus);

        btn = mainUi.findChild("btEnable");
        btn['toggled(bool)'].connect(EnablePrSet);

        btn = mainUi.findChild("tickBox");
        btn['toggled(bool)'].connect(tickBoxToggled);

        btn = mainUi.findChild("btOperation");
        btn['toggled(bool)'].connect(operateFunct);


        // BTN Save
        btn = mainUi.findChild('btSave');
        btn['clicked()'].connect(save);

 }

function rangeChanged (iRange) {
        var rangeIfs = 
            [2e-9, 6e-9, 2e-8, 6e-8, 2e-7, 6e-7, 
            2e-6, 6e-6, 2e-5, 6e-5, 2e-4, 6e-4,
            2e-3, 6e-3, 2e-2];
        //var loop = qdaq.findChild('beamLoop');
        var Ib = qdaq.findChild('Ib');
        var IbSum = qdaq.findChild('IbSum')
        var dt = 1; // 1s loop period
        Ib.multiplier = rangeIfs[iRange]*1e9/100./dt;
        //IbSum.multiplier = rangeIfs[iRange]*1e9/100./dt;
}

function updatePlot () {
     var mainUi = ui.mainUi;
        var plot1 = mainUi.findChild("plotIbvst");

        var btRt = mainUi.findChild('btRt');

        var D;
        if (btRt.checked) D = qdaq.findChild('rtData');
        else D = qdaq.findChild('bfData');

        plot1.clear();
        plot1.setTimeScaleX();
        plot1.plot(D.t,D.Ib);

}

function updateStatus(){
    var mainUi = ui.mainUi;
    var beamctr = qdaq.findChild('beamctr');  
    var btBeamOn = mainUi.findChild('btBeamOn');
    var mb = qdaq.findChild("mb");
    if(beamctr.beamOn != btBeamOn.checked){btBeamOn.toggle();}
    var StatusMsg = ui.mainUi.findChild("edtStatus");
    StatusMsg.text = mb.isOpen ? "Connected" : "Disconnected";

}

function start(on) {
    //var beamUi = ui.mainUi.findChild('beamCtrl');
    var mainLoop = qdaq.findChild('mainLoop');
    var mainUi = ui.mainUi;
    var beamctr = qdaq.findChild('beamctr');    
    var Ib = qdaq.findChild('Ib');

    if (on) {

        log('in start/on=1')

        var cb = ui.mainUi.findChild('edtLJip');
        var ipaddr = cb.text;


        cb = mainUi.findChild('cbRange');
        var rangeIdx = cb.currentIndex;
       
        beamctr.off();
        beamctr.interface.close();
        beamctr.interface.host = ipaddr;
        
    


        if (!beamctr.interface.open()) {throw("Cannot open MODBUS TCP connection to LabJack beam counter."); StatusMsg.text = "modbus error"};
        if (!beamctr.on()) {throw("Cannot open LabJack beam counter."); StatusMsg.text = "modbus error"};

        var buff = beamctr.read(3000,2); // read once to set initial value of PrevCnts
        log('got 1st buff size = ' + buff.length)
        beamctr.PrevCnts = buff.readUInt16LE(2) + buff.readUInt16LE()*65536;

        beamctr.runCode = textLoad('js/run.js');

        rangeChanged(rangeIdx);

        qdaq.mainLoop.arm();
        
             
    } else {
        mainLoop.disarm();
        var StatusMsg = ui.mainUi.findChild("edtStatus");
        StatusMsg.text = 'Disconnected';
        this.operateFunct(0);
    }
}

function reset(){
    qdaq.mainLoop.beamctr.TotalCnts = 0;
    qdaq.mainLoop.bfData.clear();
    qdaq.mainLoop.rtData.clear();
}


function beamOn(on) {
    var beamctr = qdaq.findChild('beamctr');
    beamctr.beamOn = on ? 1 : 0;
    beamctr.write(2002,beamctr.beamOn);
        
}


function EnablePrSet(on){
    var beamctr = qdaq.findChild('beamctr');
    beamctr.presetEnable = on ;
    var w = ui.mainUi.findChild("PrSet");
    beamctr.presetCounts = w.value;
    w.readOnly = on;
}

function tickBoxToggled(on){
    var beamctr = qdaq.findChild('beamctr');
    var btEnable = ui.mainUi.findChild('btEnable');
    if(!on) {
        beamctr.presetEnable = 0 ;
        btEnable.checked = 0;
    }

}

function operateFunct(on){
    var mainUi = ui.mainUi;
    var  btOperation = mainUi.findChild("btOperation");
    var btBeamOn = mainUi.findChild('btBeamOn')
    if(!on){
        btBeamOn.setChecked(0);
        btOperation.setChecked(0);
    }
    

}

function create_popupui(){
    var Ui_popup = ui.loadTopLevelUi('js/main_popup.ui','Ui_popup');
}

function popup_funct(){
    var Ui_popup = ui.Ui_popup;
   // Ui_popup.size= 0;
    Ui_popup.show();
    var OkButton = Ui_popup.findChild("OkBtn");
    OkButton['clicked(bool)'].connect(close_popup);
}

function close_popup(){
    var Ui_popup = ui.Ui_popup;
    Ui_popup.hide()
}

function save()  {
    var i = textLoad(appPath + "./data/.autoNumber");
        i++;
        textSave(i.toString(),appPath + "./data/.autoNumber");
        var fname = appPath + "../data/" + ("000" + i).slice (-4) + ".h5";
    //    var timestamp = Date().toString();

        
    //    var myui = ui.mainUi.findChild('dataCtrl');

        h5write(qdaq,fname);
    var bfData = qdaq.mainLoop.bfData;
   // i++;
    //textSave(bfData.toString(), "./data/.autoNumber");
    //var fname = "./data/" + ("000" + i).slice (-4) + ".txt";
    //var timestamp = Date().toString();

    //qdaq.mainLoop.comment1 = myui.findChild('edtComment1').plainText;
    //qdaq.mainLoop.comment2 = myui.findChild('edtComment2').plainText;

    //h5write(qdaq,fname);
   
}


importExtension("qdaq-filters")
importExtension("qdaq-interfaces")
create();
createUi();
create_popupui();
qdaq.mainLoop.arm();
// mainCtrl.arm(1);

ui.mainUi.show();









