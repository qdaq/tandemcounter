# TandemCounter

This application is used for opening and closing the beam Farraday Cup and perform beam counting on the NCSR Tandem laboratory.


 <span style="font-size:1.5em;">**1.  _Connection_**</span>

First we have to connect to the TANDEM console. The connection is over ethernet at the ip address 10.0.5.14.

<ul>
<li>Give the ip address in box "LabJack IP" and press "Connect"</li>
<li>A  "Connected" message appears and green led starts flashing</li>
</ul> 

Now we are connected to the TANDEM console. In order to be able to operate the ion beam (open/close, counting etc) the **_"Operation"_ box** must be ticked.

<span style="font-size:1.5em;">**2. _Beam Current Measurement_**</span>

- Tick the ***"Operation"*** box
- Select the beam current scale (must correspond to the scale of the console's Current Integrator)
- The plot shows a trace of the beam current over time. If "RealTime" is selected, the plot shows the last 5 min. If "Buffer" is selected, the plot shows the current trace since the last "Reset"
- "Reset" clears all measurements from memory  

> 
> BEAM CURRENT READING IS CORRECT ONLY IF THE SCALE IS IN ACCORDANCE WITH TANDEM'S CONSOLE CURRENT SCALE. 
>


<span style="font-size:1.5em;">3. **_Counter Operation_**</span>
 
<ul>
<li>Tick the **_"Operation"_ box**</li>
<li>The program starts counting from the console's CURRENT INTEGRATOR output</li>
<li>100 counts/s correspond to the CURRENT INTEGRATOR's full scale current</li>
<li>The "Total Counts" box displays the current total count sum since the last "Reset"</li>
<li>"Reset" zeroes out the "Total Counts" reading</li>
</ul> 

In order to perform irradiation up to a desired number of counts and then turn the beam off, the "Preset" option must be enabled.

<ul>
<li> Tick "Preset" box</li>
<li> Type desired number of counts in the box </li>
<li>Press "Enable" to activate the "Preset" function</li>
<li>Once the "Total Counts" become equal to "Preset" counts the beam is turned off</li>
<li>The automatic beam operation has to be enabled (see below)</li>
</ul> 

<span style="font-size:1.5em;">4. **_Beam On/Off_**</span>

<ul>
<li>Tick the **_"Operation"_ box**</li>
<li>Enable automatic beam operation at the TANDEM console</li>
<li>Toggle the "Beam On/Off" button to switch the beam on and off</li>
</ul>

<span style="font-size:1.5em;">5. **Troubleshooting**</span>

- *Connection is not possible*: 
  - Check the IP address. 
  - Check that the [LabJack T7 - Pro](https://labjack.com/products/labjack-t7-pro) device is powered-on (situated behind CURRENT INTEGRATOR)
- *Beam On/Off does not work*: 
  - Check that automatic beam control is enabled at the TANDEM console
  - Check if the preset function is "Enabled" and "Total Counts" have reached preset counts 
