var mainLoop = qdaq.appendChild(new QDaqLoop("mainLoop"));
        mainLoop.createLoopEngine(); // main loop script engine

var mb = mainLoop.appendChild(new QDaqModbusTcp("mb"));
mb.host = "10.10.0.151";
mb.port = 502;
// create beam counter device
var beamctr = mainLoop.appendChild(new QDaqDevice("beamctr"));
beamctr.interface = mb;
var Ib = beamctr.appendChild(new QDaqChannel("Ib"));
beamctr.interface.open()
beamctr.on();
beamctr.read(3100,2); var prevCts = 0;
while(1){
    var buff = beamctr.read(3000,2);
    log('buff0= ' + buff[0] + " /buff1= " + buff[1] +" /buff2 " + buff[2] + " /buff3 " + buff[3] + " UINT32BE = " + buff.readUInt16LE());
    var counts = buff.readUInt16LE(2) + buff.readUInt16LE()*65536;
    log("counts= " + counts + "  DC = " + (counts - prevCts));
    prevCts = counts;
    wait(1000);
    
}