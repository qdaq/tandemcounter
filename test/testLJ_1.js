//test script for LabJack T7
importExtension("qdaq-filters")
importExtension("qdaq-interfaces")

function create() {
    var mainLoop = qdaq.appendChild(new QDaqLoop ("mainLoop"));
    mainLoop.createLoopEngine();
    mainLoop.period = 1000;

    var t = mainLoop.appendChild(new QDaqChannel("t"));
        t.type = "Clock";
        t.format = "Time";
        t.signalName = 'meas. time';
        t.unit = 's';

   // create modbus connection to counter device with modbus ifc
    var mb = mainLoop.appendChild(new QDaqModbusTcp("mb"));
    mb.host = "10.0.5.14";
    mb.port = 502;
    // create beam counter device
    var beamctr = mainLoop.appendChild(new QDaqDevice("beamctr"));
    beamctr.interface = mb;   
    
    // create counter channel
    var Ib = beamctr.appendChild(new QDaqChannel("Ib"));
    //Ib.type = 'Random';
    Ib.format = 'FixedPoint';
    Ib.digits = 2;
}

function createUi() {
    var mainLoop = qdaq.findChild("mainLoop");
    var mainUi = ui.loadTopLevelUi('js/mainLJ_1.ui','mainUi');

    var btn = mainUi.findChild('btnStart'); 
    btn['toggled(bool)'].connect(start); 

    ui.bind(mainLoop.beamctr.Ib,mainUi.findChild('Ib'));
}

function arm(on) {
    var loop = qdaq.mainLoop;

    if (on) {
        
        loop.arm();
    }
    else {

        loop.disarm();

             }
}

function start(on) {
    //var beamUi = ui.mainUi.findChild('beamCtrl');
    var mainLoop = qdaq.findChild('mainLoop');
    var mainUi = ui.mainUi;
    var beamctr = qdaq.findChild('beamctr');    
    var Ib = qdaq.findChild('Ib');
    log('hahaha')

    if (on) {
    var cb = ui.mainUi.findChild('edtLJip');
    var ipaddr = cb.text;

    beamctr.interface.close();
    beamctr.interface.host = ipaddr;
    if (!beamctr.interface.open()) throw("Cannot open MODBUS TCP connection to LabJack beam counter.");
    if (!beamctr.on()) throw("Cannot open LabJack beam counter.");

    beamctr.read(3100,2); // read once to reset counts

    beamctr.runCode = textLoad('js/run.js');

    mainLoop.arm();
    } else {
        mainLoop.disarm();
    }
}

create();
createUi();
ui.mainUi.show();