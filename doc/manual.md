# NCSRD Tandem accelerator Beam Operation

Users of the NCSRD Tandem accelerator can utilize the Beam Operation system to perform the following tasks:

- **Turn the ion beam on or off** remotely from a control computer, either by user action or automatically after a specified integrated total beam charge has been acheived 
- Get a **real-time beam current measurement** and a plot as a function of time
- Get an indication for the **integrated total ion beam charge** during an irradiation
- Set a **Preset** total beam charge after which the beam is automatically switched off

The Beam Operation system comprises of the following components:

1. The [TandemCounter application](https://gitlab.com/qdaq/tandemcounter) which can be downloaded  and installed on any computer.
2. The [LabJack T7 - Pro](https://labjack.com/products/labjack-t7-pro), multifunction DAQ device. 
3. The **Current Integrator** and the **beam control switch circuit** at the Tandem console.

This manual describes in detail the connections and operation of the various components as well as the TandemCounter application. A quick guide for the app can be found [here](https://gitlab.com/qdaq/tandemcounter/-/blob/main/js/README.md?ref_type=heads), or by selecting the "About" option from inside the actual application. 

The Manual is divided in the following sections:

1. **Physical Connections**
2. **LabJack T7 details**


## Physical Connections
- **Beam on/off:** 
  
  The ion beam is switched on and off by a Faraday cup (FC) located inside the accelerator's hall. The FC is operated by a 24 V DC voltage supplied from the Tandem console. The beam control switch (see the schematic) selects the mode of operation:
  - In *"Manual"* mode the 24 VDC are passed directly to the accelerator and the FC is controlled by the Tandem operator
  - In *"Auto"* mode the 24 VDC supply is controlled by a [BMY relay switch](https://www.beta.com.tw/products_detail/57.htm) operated by a 3.3 V TTL voltage from LabJack-T7. Thus, a control computer running the **TandemCounter** app can connect to LabJack-T7 and operate the beam.

- **Beam current measurement / Counter**
  
  The ion beam current from an experiment is typically sent to the console's Current Integrator (CI). This instrument has a digital output which gives TTL pulses at a frequency proportional to the measured current. A pulse frequency of 100Hz corresponds to the full-scale current reading. The digital output from the CI is driven to LabJack's FIO0 digital input channel, which is configured to operate as a pulse counter. The pulse count measurements are transmitted to the **TandemCounter** application and converted to beam current.

The connection between computer and LabJack is through the common ethernet network.
The **LabJack IP address is 10.0.5.14**.

The connections are shown in the following schematic:

![Diagramm](draft_electric_design.png)
*Wiring diagramm*


## LabJack T7-Pro

The Labjack T7-Pro, a multifunctional DAQ, acts as a Modbus-TCP server. Modbus-TCP is the protocol that describes how to interpret the data stream. A server will wait for commands to be sent to it from a client. A
computer acts as the client when making requests of the server. 

In our case, a computer running the **TandemCounter** app is the client. 

LabJack also provides its own gui software which is named "Kipling" and can be downloaded from [here](https://labjack.com/pages/support?doc=/software-driver/labjack-applications/kipling/#:~:text=Kipling%20is%20a%20configuration). Kipling provides access to all LabJack functions, input/output and configuration options.

Apart from Modbus-TCP the device can be connected by USB and WiFi and can be programmed directly by the dedicated open-source and cross-platform library LJM.

### Modbus Protocol details
Modbus TCP is a register based protocol. Registers store values which can be read from or
written to. Each register represents a configuration option, a measurement, or a variable. In the LabJack device registers are both readable and writable.

All of the Modbus registers that are supported are listed in the so-called [Modbus Map](https://files.labjack.com/datasheets/LabJack-T-Series-Datasheet.pdf#file%3A///root/book.html.1.html%23docsie-title-7). The map allows users to quickly look up any available operations.

The word “register” is often used with two different meanings. The Modbus protocol defines a
register as a single 16-bit value which resides at a single address within the Modbus map. A
register can also be defined as a logical value. The logical value may require larger data-types
such as a 32-bit floating point number. If the data-type associated with a logical value is larger
than 16-bits, then multiple Modbus TCP registers will need to be read sequentially. Therefore, in our application, most **"registers" consist of 1 or 2 "Modbus registers"**.

LabJack T7-pro has  a single map of addresses from 0 to 65535. Any type of register can be
located anywhere in that address range, regardless of the data type or whether it is read-only,
write-only, or read-write. Some addresses point to buffers that can access more data than what would normally fit
within the Modbus address range. See [Buffer Registers](https://files.labjack.com/datasheets/LabJack-T-Series-Datasheet.pdf#file%3A///root/book.html.1.html%23section-header-two-cm3lk).

In-depth information about Modbus TCP can be found at [modbus.org](https://modbus.org/) and a summary of all LabJack Modbus protocol implementation details can be found in [LabJack protocol Details](https://labjack.com/pages/support?doc=%2Fsoftware-driver%2Fdirect-modbus-tcp%2Fprotocol-details%2F).


### Physical Connection and Programming
In current application, two actions are performed: 
- opening and closing beam farraday cup - **"Beam ON/OFF"**
- counting events during irradiation - **"Counter"**

**Physical connections** and their interpretation according to LJM Library and Modbus TCP protocol are:

![LabJack Physical Connection](Labjack_overview.resized.png)
*LabJack T7 physical connections*
![LabJack Block diagramm](block_diagramm.resized.png)
*LabJack Block Diagramm*

  
| Operation  |Channel| LJM Library name | Modbus Address  | Type | Access |
| ----------- | ------- | -------- | -------- | ----------- | ------|
|Beam On/Off|FIO2|  FIO2/ DIO2 | 2002 | UINT16 | R/W|
|Counter| FIO0 | DIO0_EF_READ_A | 3000 | UINT32| R |

In case of Beam On/Off, the output of digital channel FIO2 is 0 or 3.3 V depending on the value of MODBUS register #2002, 0 or 1, respectively.

In case of the counter, the number of pulses counted at the input channel FIO0 can be read from MODBUS address 3000 as a 32-bit unsigned integer. The maximum supported pulse rate is 20kHz.

#### Programming
Programming full details can be found in next section. Nevertheless, a short description on the implementation of these actions inside our software is given, as part of connection chapter. 

**TandemCounter** application is a javascript file on top of [qdaq](https://gitlab.com/qdaq) framework, thus taking advantage of objects and jobs created within. Omitting details regarding objects' hierarchy and referring only to "*beamctr*" which is a QDaq Device with ModBus TCP interface, two aforementioned main jobs are performed as follows in short:
1. Create **beamctr** QDaq Device
2. Define its **Interface**, meaning its digital communication, as Modbus TCP protocol and create connection with **host the ip address** and **port number = 502**.
3. **_Beam ON/OFF_** : Communicate with Digital Input/Output channel DIO2 by writing beam status (0/1) at its register (according to previous table). 
> beamctr.write(2002,0/1)
4. **Counter**: Initially read if there are any previous counts. Then keep reading counts (read by LabJack) and keep track of previous counts, current counts and total counts so far. These tasks arre performed as follows:
> var buff = beamctr.read(3000,2); // read once to set initial value of PrevCnts
>beamctr.PrevCnts = buff.readUInt16LE(2) + buff.readUInt16LE()*65536;

> [ ....... ]
> 
> if(buff.length == 4){
    var counts = buff.readUInt16LE(2) + buff.readUInt16LE()*65536;
    DC = counts - mb.PrevCnts;
    mb.PrevCnts = counts;
}
>mb.TotalCnts += DC;






