# TandemCounter

Application for user operation of the ion beam at the NCSR Tandem laboratory.

A short description of the application can be found here: [js/README.md](./js/README.md)

Detailed information on the beam control system is given in the manual, [./doc/manual.md](./doc/manual.md)

